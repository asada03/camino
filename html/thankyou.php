<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>	
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Camino a ser libre - hoja de registro</title>

<html>
<head>
<link rel="stylesheet" href="camino.css" type="text/css" />
</head>
<body>

<div class="container">
	<?php include("encabezado.php"); ?>
 	<div class="content">
		<h1><span class="standout"> &iexcl;Espera! solo falta un paso.</span></h1><br/>
		<p>Para poder enviarte m&aacute;s informaci&oacute;n, es necesario confirmar tu email. </p>
		<p>En unos instantes recibir&aacute;s un email en tu buz&oacute;n de correo electr&oacute;nico con la siguiente informaci&oacute;n:</p>
		<span class="note">
		<table align="center" style="margin-bottom:15px">
		<tr>
		  <td style="text-align:right; padding-right:4px">de:</td>
		  <td>Camino a ser Libre</td>
		</tr>
		  <td style="text-align:right; padding-right:4px">asunto:</td>
		  <td>TU RESPUESTA ES REQUERIDA: para confirmar tu solicitud</td>
		<tr>
		</tr>
		</table>
		</span>
		<p>Para recibir toda la informaci&oacute;n de este proyecto, entra a este email y da click en el link que se encuentra abajo de donde dice &quot;CONFIRM BY VISITING THE LINK BELOW:&quot;</p>
		<p>Este es un ejemplo del email que recibiras y donde debes de dar click: </p>
		<p><img src="../imagenes/Ejemp Email.jpg" style="width:360; height:233; margin:35px 0 35px 0;" /></p>
		<p>Una vez hecho esto, se te enviar&aacute; directamente al portal donde ver&aacute;s toda la informaci&oacute;n.</p>
		<p><span class="note">Nota: Si en unos minutos no has recibido este email, por favor b&uacute;scalo en tu buz&oacute;n de correo no deseado. Si a&uacute;n no lo encuentras manda un email a soporte@caminoaserlibre.com para resolver esta situaci&oacute;n.</span>
	</div>
	<div class="footer">Copyright 2009-2010 www.caminoaserlibre.com	</div>
</div>

<!-- Google Analithics -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-12286738-1");
pageTracker._trackPageview();
} catch(err) {}</script>
<!-- Fin Google Analithics -->
      
</body>
</html>
