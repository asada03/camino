<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>	
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Camino a ser libre - hoja de registro</title>

<html>
<head>
<link rel="stylesheet" href="camino.css" type="text/css" />
</head>
<body>

<div class="container">
	<?php include("encabezado.php"); ?>
 	<div class="content" style="padding-left:15%; padding-right:15%;">
		<h1><span class="standout">&iexcl;Direcci&oacute;n de email duplicada!</span></h1>
		<p>Te pedimos una disculpa, pero tu direcci&oacute;n de email ya estaba registrada en nuestro sistema y no podemos registrarla de nuevo.</p>
		<p>Si deseas entrar una vez m&aacute;s al portal, por favor abre al email de bienvenida que te enviamos y ah&iacute; encontrar&aacute;s el link de acceso. </p><br />
		<span class="note"><p>Si ah&uacute;n no has recibido tu email de bienvenida, b�scalo en tu buz�n de correos no deseados. Si a&uacute;n no lo puedes encontrar o ya lo borraste, manda un email a soporte@caminoaserlibre.com y explica la situaci&oacute;n, con gusto te ayudar&aacute;n a resolver la situaci�n.</p></span>
		<p>&nbsp;</p><p>&nbsp;</p>
	</div>
	<div class="footer">Copyright 2009-2010 www.caminoaserlibre.com	</div>
</div>

<!-- Google Analithics -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-12286738-1");
pageTracker._trackPageview();
} catch(err) {}</script>
<!-- Fin Google Analithics -->
      
</body>
</html>

