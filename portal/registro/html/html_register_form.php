<html>
<head>
	<title>Hoja de Registro</title>
	<script type="text/javascript" src="sha1.js"></script>
	<script type="text/javascript" src="login.js"></script>
	<link rel="stylesheet" href="http://www.caminoaserlibre.com/html/camino.css" type="text/css" />
    <style type="text/css">
    </style>
</head>
<body>
<div class="container">
	<?php include("../../html/encabezado.php"); ?>
	<div class="content">
				<span class="standout"><?php print $alert; ?></span>
			<h1>Registrate al portal de Camino a ser Libre</h1>
			<form name="register" action="<?php print htmlentities($_SERVER['PHP_SELF']); ?>" method="post" onSubmit="javascript:doRegister(this);">
			<table width=60% align="center">
				<tr><td colspan="2"><h3>Proporciona tus datos personales</h3></td></tr>
				<tr><td>Nombre:<span class="standout">*</span></td>
					<td><input type="text" name="name" value="<?php printField('name'); ?>" maxlength="40"></td>
				</tr>
				<tr><td>Apellidos:<span class="standout">*</span></td>
					<td><input type="text" name="lastname" value="<?php printField('lastname'); ?>" maxlength="40"></td>
				</tr>
				<tr><td>Ciudad:</td>
					<td><input type="text" name="city" value="<?php printField('city'); ?>" maxlength="30"></td>
				</tr>
				<tr><td>Estado:</td>
					<td><input type="text" name="state" value="<?php printField('state'); ?>" maxlength="30"></td>
				</tr>
				<tr><td>Direcci�n de Email:<span class="standout">*</span></td>
					<td><input type="text" name="email" value="<?php printField('email'); ?>" maxlength="140"></td>
				</tr>
				<tr><td>Tel�fono Principal:<span class="standout">*</span></td>
					<td><input type="text" name="mainphone" value="<?php printField('mainphone'); ?>" maxlength="20"></td>
				</tr>
				<tr><td>Tel�fono Celular:</td>
					<td><input type="text" name="cellphone" value="<?php printField('cellphone'); ?>" maxlength="20"></td>
				</tr>
				<tr><td>Otro Tel�fono:</td>
					<td><input type="text" name="otherphone" value="<?php printField('otherphone'); ?>" maxlength="20"></td>
				</tr>
				<tr><td>�Como te gusta que te digan?:<span class="standout">*</span></td>
					<td><input type="text" name="nickname" value="<?php printField('nickname'); ?>" maxlength="30"></td>
				</tr>
				<tr><td colspan="2"><br><h3>Proporciona tus datos de Une-t</h3></td>
				</tr>
				<tr><td>Numero Usuario Une-t:<span class="standout">*</span></td>
					<td><input type="int" name="idunet" value="<?php printField('idunet'); ?>" ></td>
				</tr>
				<tr><td>Numero Usuario de tu Patrocinador:</td>
					<td><input type="int" name="idsponsor" value="<?php printField('idsponsor'); ?>" ></td>
				</tr>
				<tr><td colspan="2"><br><h3>Escoge un Nombre de Usuario y Contrase�a</h3>
					<span class="note">El Nombre de Usuario ser� utilizado para crear tu nueva p�gina de prospectaci�n (ej. www.caminoaserlibre.com/ricardo)<spam></td>
				</tr>
				<tr><td>Usuario:<span class="standout">*</span></td>
					<td><input type="text" name="user" value="<?php printField('user'); ?>" maxlength="30"></td>
				</tr>
				<tr><td>Contrase�a:<span class="standout">*</span></td>
					<td><input type="password" name="pass_field_1" maxlength="30"><input type="hidden" name="pass1" value="" /></td>
					</tr>
				<tr><td>Reescribir Contrase�a:</td>
					<td><input type="password" name="pass_field_2" maxlength="30"><input type="hidden" name="pass2" value="" /></td>
				</tr>
		<?php
		if (CAPTCHA) {
		?>
				<tr><td colspan="2"><br><h3>Introduce este c�digo de seguridad</h3></td>
				</tr>
				<tr><td style="text-align:right">Codigo de Seguridad ---></td>
					<td><img src="captcha.php" alt="CAPTCHA image" width="60" height="20" /></td>
				</tr>
				<tr><td>Escribir Codigo de Seguridad:<span class="standout">*</span></td>
					<td><input type="text" name="validator" id="validator" size="4" /></td>
				</tr>
		<?php
		}
		?>
				<tr><td colspan="2" style="text-align:right;">
							<input type="hidden" name="salt" value="<?php print $salt; ?>" />
							<input type="hidden" name="key" value="<?php print $_SESSION['key']; ?>" />
							<input type="submit" name="subform" value="Registrarme">
					</td>
				</tr>
				</table>
			</form>
	</div>
	<div class="footer">Copyright 2009-2010 www.caminoaserlibre.com
	</div>
</div>

<!--- Google Analithics -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-12286738-2");
pageTracker._trackPageview();
} catch(err) {}</script>
<!--- Google Analithics -->
</body>
</html>