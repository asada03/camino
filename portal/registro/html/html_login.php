<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>	
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Camino a ser libre - hoja de registro</title>

<html>
<head>
	<script type="text/javascript" src="sha1.js"></script>
	<script type="text/javascript" src="login.js"></script>
	<link rel="stylesheet" href="http://www.caminoaserlibre.com/html/camino.css" type="text/css" />
</head>
<body>

<div class="container">
	<?php include("../../html/encabezado.php"); ?>
 	<div class="content">
		<?php print $alert; ?>
		<h1>�Ya tienes tu cuenta para ingresar al portal?</h1>
		<div class="loginform">
			<h2>Iniciar Sesion</h2>
			<form name="login" action="" method="post" onSubmit="return doLogin(this)">
			<table align="center">
				<tr>
					<td align="left">Usuario:</td>
					<td><input type="text" name="user" maxlength="30"></td></tr>
				<tr>
					<td align="left">Contrase�a:</td>
					<td><input type="password" name="pass_field"></td></tr>
				<tr>
					<td colspan="2" align="left">
						<input type="checkbox" name="remember">
						<font size="2">Recordarme en esta computadora</font>					</td>
				</tr>
				<tr>
					<td colspan="2" style="text-align:right;">
						<input type="hidden" name="pass" value="" />
						<input type="hidden" name="salt" value="<?php print $salt; ?>" />
						<input type="hidden" name="key" value="<?php print $_SESSION['key']; ?>" />
						<input type="submit" name="subform" value="Iniciar Sesi�n">					</td>
				</tr>
			</table>
			</form>
		</div>
		<div class="signup">
				<table>
				<tr><td><h2>Para crear una nueva cuenta da <a href="http://www.caminoaserlibre.com/portal/registro/register.php">Click Aqu�</a>.</h2>
				
				</td></tr></table>
		</div>
		<p><div style="margin-left:45px;" align="left"><a href="forgot.php">Olvide mi contrase�a</a></p></div></p>
		<p>&nbsp;</p><p>&nbsp;</p>
 	</div>
	<div class="footer">Copyright 2009-2010 www.caminoaserlibre.com	</div>
</div>

<!--- Google Analithics -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-12286738-2");
pageTracker._trackPageview();
} catch(err) {}</script>
<!--- Google Analithics -->
</body>
</html>