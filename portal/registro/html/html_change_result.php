<html>
<head>
	<title>Resultado Cambio a mi cuenta</title>
	<link rel="stylesheet" href="http://www.caminoaserlibre.com/html/camino.css" type="text/css" />
    <style type="text/css">
    </style>
</head>
<body>
<div class="container">
	<?php include("../../html/encabezado.php"); ?>
	<?php include("../loginfo.php");?>
	<?php include("../menu.php"); ?>
	<div class="content">
		<?php
		
		if($_SESSION['regresult']) {
		
		?>
			<h1>�Tus cambios se han realizado!</h1>
			<p>Muchas gracias <b><?php echo $_SESSION['name'] . " " . $_SESSION['lastname'] ; ?></b>, tus datos han sido modificados.<br /></p>
		<?php
		
		} else {
		
		?>
			<h1>El intento para cambiar tus datos ha fallado</h1>
			<p>
				Lo sentimos, pero ha ocurrido un error en el sistema al intentar cambiar los datos.<br />
				Tus datos no han cambiado, por favor intentalo de nuevo.<br />
			</p>
		<?php
		
		}
		
		?>
	</div>
	<div class="footer">Copyright 2009-2010 www.caminoaserlibre.com
	</div>
</div>

<!--- Google Analithics -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-12286738-2");
pageTracker._trackPageview();
} catch(err) {}</script>
<!--- Google Analithics -->
</body>
</html>
