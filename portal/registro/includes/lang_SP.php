<?php

// English laguage file
// Contains all text wich is not in the HTML files

$ALERT['PASS_NO'] = 'Escribe una contrase�a.';
$ALERT['PASS_CURR_NO'] = 'Si deseas cambiar tu contrase�a debes escribir tu contrase�a actual.';
$ALERT['PASS_CURR_WRONG'] = 'La contrase�a actual no es correcta, por favor intenta de nuevo.';
$ALERT['PASS_DIFF'] = 'Las contrase�as escritas son diferentes, por favor escribelas de nuevo.';
$ALERT['PASS_TOLONG'] = 'La contrase�a es mayor a 30 caracteres, por favor escoje una contrase�a m�s corta.';
$ALERT['PASS_TOSHORT'] = 'La contrase�a es menor a 6 caracteres, por favor escoje una contrase�a m�s larga.';

$ALERT['USER_NO'] = 'Escribe un nombre de usuario.';
$ALERT['USER_TOLONG'] = 'El nombre de usuario es mayor a 30 caracteres, por favor escoje uno m�s corto.';
$ALERT['USER_TOSHORT'] = 'El nombre de usuario es menor a 3 caracteres, por favor  escoje uno m�s largo.';
$ALERT['USER_TAKEN'] = 'Este usuario ya existe, por favor escoje un nombre de usuario diferente.';
$ALERT['USER_NOTALLOWED'] = 'Este nombre de usuario no es permitido, por favor escoje uno diferente.';

$ALERT['NAME_NO'] = 'Por favor escribe tu nombre.';
$ALERT['LASTNAME_NO'] = 'Por favor escribe tu apellido.';
$ALERT['MAINPHONE_NO'] = 'Por favor escribe tu tel�fono principal.';
$ALERT['NICKNAME_NO'] = 'Por favor indicanos como te gusta que te digan.';
$ALERT['IDUNET_NO'] = 'Por favor proporciona tu n�mero de usuario Une-t.';
$ALERT['IDSPONSOR_NO'] = 'Por favor indicanos el n�mero de usuario de tu patrocinador.';

$ALERT['EMAIL_NO'] = 'Escribe tu direcci�n de correo electr�nico.';
$ALERT['EMAIL_TOLONG'] = 'La direcci�n de correo electr�nico no debe ser mayor a 140 caracteres.';
$ALERT['EMAIL_INVALID'] = 'La direcci�n de correo electr�nico no es valida, por favor introducela de nuevo.';
$ALERT['EMAIL_TAKEN'] = 'Esta direcci�n de correo electr�nica ya existe en elsistema, por favor elige otra.';
$ALERT['EMAIL_NOTEXIST'] = 'Esta direcci�n de correo electr�nico no existe en el sistema por favor verif�cala.';
$ALERT['EMAIL_ALREADYSENT'] = 'Un correo electr�nico con instrucciones de como obtener una nueva contrase�a ya ha sido enviado a tu direcci�n de correo electr�nico';
$ALERT['EMAIL_SENT_FORGOT'] = 'Se te ha enviado un correo electr�nico con instruciones de coo obtener una nueva contrase�a';
$ALERT['EMAIL_SENT_ERROR'] = 'Ha ocurrido un error y no se te ha enviado el correo electr�nico.';

$ALERT['PAGE_UNAV'] = 'Te pedimos disculpas, la p�gina solicitada no esta disponible en este momento.';
$ALERT['ERROR'] = 'Una disculpa, esta acci�n no pudo ser realizada...';
$ALERT['CAPTCHA'] = 'No llenaste correctamente el codigo de seguridad';

$MAILTEXT['FORGOT_SUBJECT'] = 'Informaci�n para recobrar la contrase�a';
$MAILTEXT['FORGOT_BODY'] = "Da click en este link para crear una nueva contrase�a:\r\n"; // This string is enclosed in double-quotes, so you can use \r\n to start a new line.
$MAILTEXT['FORGOT_FOOTER'] = "Que tengas un buen dia.";

?>