<?php

// A string used to encrypt the passwords, can be anything.
// This should not be changed once the login system is operating.
$encryption_string = "jgffsadagovelaHHGrrFdeEe";

// Use trailing slashes with all paths

define('DB_HOST', 'db2176.perfora.net');
define('DB_NAME', 'db310997589');
define('DB_USERNAME', 'dbo310997589');
define('DB_PASSWORD', 'ptuanio03h');
define('DB_PREFIX', ''); // Not required

define('LANGUAGE', 'SP'); // Currently available: EN (English) and NL (Dutch), or create your own!
define('EMAIL', 'soporte@caminoaserlibre.com');

define('LOGIN_URL', 'http://portal.caminoaserlibre.com/registro/');
define('SUCCESS_URL', 'http://portal.caminoaserlibre.com'); // URL to go to after successfull login

// If ALLOW_RESET is set to false, users cannot reset their password when they have forgotten it.
// This is offcourse less user friendly, but also more secure,
// since the e-mail containing the reset variable could be sniffed.
define('ALLOW_RESET', true);

// If ALLOW_JOIN is set to true, people can register for a login name and password.
define('ALLOW_JOIN', true);

// If CAPTCHA is set to true, people need to enter a CAPTCHA to join (more secure).
define('CAPTCHA', false);

define('HTML_PATH', '/homepages/42/d106753210/htdocs/camino/portal/registro/html/'); // The absolute path.

// -- End of configuration -- //

// Connect to the mysql database.
$conn = mysql_connect(DB_HOST, DB_USERNAME, DB_PASSWORD) or die(mysql_error());
mysql_select_db(DB_NAME, $conn) or die(mysql_error());

$salt = sha1($encryption_string);

// set key used for encryption
if (!$_SESSION['key']) $_SESSION['key'] = sha1(randString());

include_once('lang_'.LANGUAGE.'.php');

// NOTHING after ?>