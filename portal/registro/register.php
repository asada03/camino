<?php

/*

 This is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This software is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this software; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 or visit www.gnu.org
 
 Code by Lutsen Stellingwerff (www.biepenlu.nl)
 
 Based on code from evolt.org by jpmaster77
 http://evolt.org/php_login_script_with_remember_me_feature

*/

session_start();
session_regenerate_id(true); // Generate new session id and delete old (PHP >= 5 only)

// registration
include_once("includes/functions.php");
include_once("includes/config.php");

// Check if the ALLOW_JOIN variable is set
if (!ALLOW_JOIN) exit($ALERT['PAGE_UNAV']);

// Inserts the given (username, password) pair into the database.
// Returns true on success, false otherwise.
function addNewUser($username, $password, $email, $name, $lastname, $city, $state, $mainphone, $cellphone, $otherphone, $nickname, $idunet, $idsponsor)
{
	global $conn;
	/* Add slashes if necessary (for query) */
	if(!get_magic_quotes_gpc()) {
		$username = addslashes($username);
		$password = addslashes($password);
		$email = addslashes($email);
	}

$q = "INSERT INTO users (username, password, email, regdate, ip, lastdate, name, lastname, city, state, mainphone, cellphone, otherphone, nickname, idunet, idsponsor) VALUES ('$username', '$password', '$email', '".date('Y-m-d  H:i:s')."', '".$_SERVER['REMOTE_ADDR']."', '".date('Y-m-d  H:i:s')."', '$name', '$lastname', '$city', '$state', '$mainphone', '$cellphone', '$otherphone', '$nickname', '$idunet', '$idsponsor')";
	return mysql_query($q,$conn);
}



// Display registration result page
if(isset($_SESSION['registered'])){

	// html
	include_once(HTML_PATH."html_register_result.php");

	unset($_SESSION['reguname']);
	unset($_SESSION['registered']);
	unset($_SESSION['regresult']);
	return;
}



// If the register form has been submitted: check for errors.
// No errors (count($alertArr) == 0)? Add record to database.
// Errors? Display error messages and show form again.
if(isset($_POST['subform'])){

	// clean up
	if ($_POST['user'])				$_POST['user'] = cleanString($_POST['user'], 30);
	if ($_POST['pass_field_1'])		$_POST['pass_field_1'] = cleanString($_POST['pass_field_1'], 30);
	if ($_POST['pass_field_2'])		$_POST['pass_field_2'] = cleanString($_POST['pass_field_2'], 30);
	if ($_POST['email'])			$_POST['email'] = cleanString($_POST['email'], 140);
	if ($_POST['pass1'])			$_POST['pass1'] = cleanString($_POST['pass1'], 40);
	if ($_POST['pass2'])			$_POST['pass2'] = cleanString($_POST['pass2'], 40);
	if ($_POST['salt'])				$_POST['salt'] = '';
	if ($_POST['key'])				$_POST['key'] = '';

	// check for errors
	$alertArr = array();

	if(!$_POST['name']) {
		$alertArr[] = $ALERT['NAME_NO'];
	}
	
	if(!$_POST['lastname']) {
		$alertArr[] = $ALERT['LASTNAME_NO'];
	}
	
	if(!$_POST['email']) {
		$alertArr[] = $ALERT['EMAIL_NO'];
	}
	else {
		
		if(strlen($_POST['email']) > 140) {
			$alertArr[] = $ALERT['EMAIL_TOLONG'];
		}
		
		if(!emailValid($_POST['email'])) {
			$alertArr[] = $ALERT['EMAIL_INVALID'];
		}
		
/*		if(emailExist($_POST['email'])) {
			$alertArr[] = $ALERT['EMAIL_TAKEN'];
		}
*/	}
	

	if(!$_POST['mainphone']) {
		$alertArr[] = $ALERT['MAINPHONE_NO'];
	}
	
	if(!$_POST['nickname']) {
		$alertArr[] = $ALERT['NICKNAME_NO'];
	}
	
		if(!is_numeric ($_POST['idunet']) || !$_POST['idunet']) {
		$alertArr[] = $ALERT['IDUNET_NO'];
	}
	
		if($_POST['idsponsor'] <> "" &&!is_numeric ($_POST['idsponsor'])) {
		$alertArr[] = $ALERT['IDSPONSOR_NO'];
	}
	
	if(!$_POST['user']) {
		$alertArr[] = $ALERT['USER_NO'];
	}
	else {
		if(strlen($_POST['user']) > 30) {
			$alertArr[] = $ALERT['USER_TOLONG'];
		}
		
		if(strlen($_POST['user']) < 3) {
			$alertArr[] = $ALERT['USER_TOSHORT'];
		}
	
		if(usernameTaken($_POST['user'])) {
			$alertArr[] = $ALERT['USER_TAKEN'];
		}
		
		if($_POST['user'] == 'flowplayer' || $_POST['user'] == 'html' || $_POST['user'] == 'imagenes' || $_POST['user'] == 'peliculas' || $_POST['user'] == 'portal'){
			$alertArr[] = $ALERT['USER_NOTALLOWED'];
		}
	}
	
	if(!$_POST['pass_field_1']) {
		$alertArr[] = $ALERT['PASS_NO'];
	}
	else {
	
		if($_POST['pass1'] != $_POST['pass2']) {
			$alertArr[] = $ALERT['PASS_DIFF'];
		}
	
		if(strlen($_POST['pass_field_1']) > 30) {
			$alertArr[] = $ALERT['PASS_TOLONG'];
		}
		
		if(strlen($_POST['pass_field_1']) < 6) {
			$alertArr[] = $ALERT['PASS_TOSHORT'];
		}
	
	}
	


	// Captcha
	if (CAPTCHA) {
		if (empty($_POST['validator']) || $_POST['validator'] != $_SESSION['rand_code']) {
			$alertArr[] = $ALERT['CAPTCHA'];
		}
		unset($_SESSION['rand_code']);
	}

	if (count($alertArr) == 0) {
		// Add the new account to the database
		// (password has already been encrypted using javascript)
		$_SESSION['reguname'] = $_POST['user'];
		$_SESSION['name'] = $_POST['name'];
		$_SESSION['lastname'] = $_POST['lastname'];
		$_SESSION['city'] = $_POST['city'];
		$_SESSION['email'] = $_POST['email'];
		$_SESSION['password1'] = $_POST['pass_field_1'];
	$_SESSION['regresult'] = addNewUser($_POST['user'], $_POST['pass1'], $_POST['email'], $_POST['name'], $_POST['lastname'], $_POST['city'], $_POST['state'], $_POST['mainphone'], $_POST['cellphone'], $_POST['otherphone'], $_POST['nickname'], $_POST['idunet'], $_POST['idsponsor']);
		$_SESSION['registered'] = true;
		$refresh = htmlentities($_SERVER[PHP_SELF]);
		exit(include_once(HTML_PATH."html_refresh.php")); // stop script
	}
}

$alert = displayAlert($alertArr);

if ($_POST['pass_field_1']) $_POST['pass_field_1'] = "";
if ($_POST['pass_field_2']) $_POST['pass_field_2'] = "";

// html sign-up form
include_once(HTML_PATH."html_register_form.php");
?>